# v0.0.1

This is the initial step of the project. It can be used as a seed project for any @angular application backed up with a Spring MVC REST backend

* Project setup
* Gitlab CI configuration
* Gradle automation