
package com.pbarrientos.vm.api.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pbarrientos.vm.api.PublicHolidaysApi;
import com.pbarrientos.vm.data.model.PublicHoliday;
import com.pbarrientos.vm.data.services.PublicHolidayService;
import com.pbarrientos.vm.exception.ApiException;
import com.pbarrientos.vm.exception.ServiceException;

/**
 * Public holidays API implementation
 *
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Component
public class PublicHolidaysApiImpl implements PublicHolidaysApi {

    @Autowired
    private PublicHolidayService publicHolidayService;

    /**
     * {@inheritDoc}
     */
    @Override
    public PublicHoliday getPublicHoliday(final Long id) throws ApiException {
        try {
            return publicHolidayService.findOne(id);
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PublicHoliday> getAllPublicHolidays() throws ApiException {
        try {
            return publicHolidayService.findAll();
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PublicHoliday> getPublicHolidaysByYear(final Integer year) throws ApiException {
        try {
            return publicHolidayService.findByYear(year);
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PublicHoliday savePublicHoliday(final PublicHoliday publicHoliday) throws ApiException {
        try {
            return publicHolidayService.save(publicHoliday);
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PublicHoliday deletePublicHoliday(final PublicHoliday publicHoliday) throws ApiException {
        try {
            publicHolidayService.delete(publicHoliday);
            return publicHoliday;
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long countPublicHolidays() throws ApiException {
        try {
            return publicHolidayService.count();
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

}
