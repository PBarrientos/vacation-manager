
package com.pbarrientos.vm.api;

import java.util.List;

import com.pbarrientos.vm.data.model.Team;
import com.pbarrientos.vm.exception.ApiException;

/**
 * Public API that manages all the teams in the application
 *
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public interface TeamsApi {

    /**
     * Counts all the teams that are in the application
     * 
     * @return the number of teams
     * @throws ApiException if there is an error performing the operation
     */
    Long countTeams() throws ApiException;

    /**
     * Get a team by its ID
     * 
     * @param teamId the ID of the team
     * @return the team
     * @throws ApiException if there is an error performing the operation
     */
    Team getTeam(Long teamId) throws ApiException;

    /**
     * Get a team by its name
     * 
     * @param teamName the team name
     * @return the team
     * @throws ApiException if there is an error performing the operation
     */
    Team getTeam(String teamName) throws ApiException;

    /**
     * Fetches all the teams that are in the application
     * 
     * @return a {@link List} with all the teams
     * @throws ApiException if there is an error performing the operation
     */
    List<Team> getAllTeams() throws ApiException;

    /**
     * Gets the team assigned to an user
     * 
     * @param userId the user ID
     * @return the assigned team, or null if the user is not assigned to any
     *         team
     * @throws ApiException if there is an error performing the operation
     */
    Team getTeamByUser(Long userId) throws ApiException;

    /**
     * Saves a new team, or modify an existing one
     * 
     * @param team the team you want to save
     * @return the saved team
     * @throws ApiException if there is an error performing the operation
     */
    Team saveTeam(Team team) throws ApiException;

    /**
     * Deletes a team from the application. This is a physical operation that
     * can't be undone.
     * 
     * @param team the Team you want to delete
     * @return the deleted team
     * @throws ApiException if there is an error performing the operation
     */
    Team deleteTeam(Team team) throws ApiException;

}
