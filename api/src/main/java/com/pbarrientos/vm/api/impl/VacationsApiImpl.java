
package com.pbarrientos.vm.api.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pbarrientos.vm.api.VacationApi;
import com.pbarrientos.vm.data.model.Vacation;
import com.pbarrientos.vm.data.services.VacationService;
import com.pbarrientos.vm.exception.ApiException;
import com.pbarrientos.vm.exception.ServiceException;

/**
 * Vacations API implementation
 *
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Component
public class VacationsApiImpl implements VacationApi {

    @Autowired
    private VacationService vacationService;

    /**
     * {@inheritDoc}
     */
    @Override
    public Vacation getVacation(final Long vacationId) throws ApiException {
        try {
            return vacationService.findOne(vacationId);
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Vacation> getAllVacations() throws ApiException {
        try {
            return vacationService.findAll();
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Vacation> getVacationsByUser(final Long userId) throws ApiException {
        try {
            return vacationService.findByUser(userId);
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Vacation> getVacationsByTeam(final Long teamId) throws ApiException {
        try {
            return vacationService.findByTeam(teamId);
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Vacation> getVacationsBetween(final LocalDate startDate, final LocalDate endDate) throws ApiException {
        try {
            return vacationService.findBetween(startDate, endDate);
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Vacation saveVacation(final Vacation vacation) throws ApiException {
        try {
            // If the vacation don't have an ID, then is a new insertion
            if (vacation.getId() != null) {
                return vacationService.save(vacation);
            } else {
                return vacationService.createVacation(vacation);
            }
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Vacation deleteVacation(final Vacation vacation) throws ApiException {
        try {
            vacationService.delete(vacation);
            return vacation;
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long countVacations() throws ApiException {
        try {
            return vacationService.count();
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

}
