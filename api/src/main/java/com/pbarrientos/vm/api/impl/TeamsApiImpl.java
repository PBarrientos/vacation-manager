
package com.pbarrientos.vm.api.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pbarrientos.vm.api.TeamsApi;
import com.pbarrientos.vm.data.model.Team;
import com.pbarrientos.vm.data.services.TeamService;
import com.pbarrientos.vm.exception.ApiException;
import com.pbarrientos.vm.exception.ServiceException;

/**
 * Teams API implementation
 *
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Component
public class TeamsApiImpl implements TeamsApi {

    @Autowired
    private TeamService teamService;

    /**
     * {@inheritDoc}
     */
    @Override
    public Team getTeam(final Long id) throws ApiException {
        try {
            return teamService.findOne(id);
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Team getTeam(final String teamName) throws ApiException {
        try {
            return teamService.findByName(teamName);
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Team> getAllTeams() throws ApiException {
        try {
            return teamService.findAll();
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Team getTeamByUser(final Long userId) throws ApiException {
        try {
            return teamService.findByUser(userId);
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Team saveTeam(final Team team) throws ApiException {
        try {
            return teamService.save(team);
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Team deleteTeam(final Team team) throws ApiException {
        try {
            teamService.delete(team);
            return team;
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long countTeams() throws ApiException {
        try {
            return teamService.count();
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

}
