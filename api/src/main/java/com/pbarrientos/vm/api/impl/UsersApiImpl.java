
package com.pbarrientos.vm.api.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.pbarrientos.vm.api.UsersApi;
import com.pbarrientos.vm.data.model.User;
import com.pbarrientos.vm.data.services.UserService;
import com.pbarrientos.vm.exception.ApiException;
import com.pbarrientos.vm.exception.ServiceException;

/**
 * Users API implementation
 *
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Component
public class UsersApiImpl implements UsersApi {

    @Autowired
    private UserService userService;

    /**
     * {@inheritDoc}
     */
    @Override
    public User getUser(final Long userId) throws ApiException {
        try {
            return userService.findOne(userId);
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User getUser(final String username) throws ApiException {
        try {
            final User user = userService.findByIdentifier(username);
            return (user != null && user.isEnabled()) ? user : null;
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<User> getAllUsers() throws ApiException {
        try {
            return userService.findAll();
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<User> getAllUsersByTeam(final Long teamId) throws ApiException {
        List<User> result = new ArrayList<>();
        try {
            result = userService.findUsersInTeam(teamId);
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User saveUser(final User user) throws ApiException {
        try {
            return userService.save(user);
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User deleteUser(final User user) throws ApiException {
        User savedUser = null;

        try {
            user.setIsEnabled(false);
            savedUser = userService.save(user);
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }

        return savedUser;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User changePassword(final User user, final String password) throws ApiException {
        if (user != null) {
            // Encode password using BCrypt algorithm
            final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            final String encodedPass = encoder.encode(password);

            user.setPassword(encodedPass);

            return saveUser(user);

        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long countUsers() throws ApiException {
        try {
            return userService.count();
        } catch (final ServiceException e) {
            throw new ApiException(e);
        }
    }

}
