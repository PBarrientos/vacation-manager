
package com.pbarrientos.vm.api;

import java.util.List;

import org.springframework.security.crypto.bcrypt.BCrypt;

import com.pbarrientos.vm.data.model.User;
import com.pbarrientos.vm.exception.ApiException;

/**
 * Public API that manages all the users in the application
 *
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public interface UsersApi {

    /**
     * Method that counts all the users in the application
     * 
     * @return the number of users
     * @throws ApiException if there is an error performing the operation
     */
    Long countUsers() throws ApiException;

    /**
     * Method that will return an user by its ID.
     * 
     * @param userId the ID of the user
     * @return the user, or null if it doesn't exist
     * @throws ApiException in case something went wrong
     */
    User getUser(Long userId) throws ApiException;

    /**
     * Method that will return an user by its user name.
     * 
     * @param username The user name
     * @return the user, or null if it doesn't exist
     * @throws ApiException in case something went wrong
     */
    User getUser(String username) throws ApiException;

    /**
     * Method that fetches all the existing users in the application
     * 
     * @return a {@link List} containing all of the users of the application.
     * @throws ApiException in case something went wrong
     */
    List<User> getAllUsers() throws ApiException;

    /**
     * Method that finds all the users that are part of a given team
     * 
     * @param teamId The team identifier
     * @return a {@link List} containing all the users of the team, or null if
     *         the team doesn't exist
     * @throws ApiException in case something went wrong
     */
    List<User> getAllUsersByTeam(Long teamId) throws ApiException;

    /**
     * Method that will save the user, wether is a new user or is a existing one
     * 
     * @param user the {@link User} to save
     * @return the saved user
     * @throws ApiException in case something went wrong
     */
    User saveUser(User user) throws ApiException;

    /**
     * Method that deletes an user from the application. Note that this is a
     * logical deletion that only will disable the user.
     * 
     * @param user the user to delete
     * @return the deleted user
     * @throws ApiException in case something went wrong
     */
    User deleteUser(User user) throws ApiException;

    /**
     * Method that will change an user password. This method should only be
     * visible to admministrators. The password is encrypted by a {@link BCrypt}
     * algorithm
     * 
     * @param user the user to change
     * @param password the plain text new password
     * @return the changed user
     * @throws ApiException in case something went wrong
     */
    User changePassword(User user, String password) throws ApiException;

}
