
package com.pbarrientos.vm.api;

import java.time.LocalDate;
import java.util.List;

import com.pbarrientos.vm.data.model.Vacation;
import com.pbarrientos.vm.exception.ApiException;

/**
 * Public API that manages all the vacations in the application
 *
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public interface VacationApi {

    /**
     * Counts all the vacations that exists in the application
     * 
     * @return the number of vacations
     * @throws ApiException if there is an error performing the operation
     */
    Long countVacations() throws ApiException;

    /**
     * Gets a vacation by its ID
     * 
     * @param vacationId the vacation ID
     * @return the vacation, or null if it doesn't exist
     * @throws ApiException if there is an error performing the operation
     */
    Vacation getVacation(Long vacationId) throws ApiException;

    /**
     * Fetches all the existing vacations
     * 
     * @return a {@link List} with all the vacations
     * @throws ApiException if there is an error performing the operation
     */
    List<Vacation> getAllVacations() throws ApiException;

    /**
     * Gets all the vacations for a given user
     * 
     * @param userId the user ID
     * @return the {@link List} of vacations for the user
     * @throws ApiException if there is an error performing the operation
     */
    List<Vacation> getVacationsByUser(Long userId) throws ApiException;

    /**
     * Gets all the vacations for a given team
     * 
     * @param teamId the team ID
     * @return a {@link List} of vacations for the team
     * @throws ApiException if there is an error performing the operation
     */
    List<Vacation> getVacationsByTeam(Long teamId) throws ApiException;

    /**
     * Fetches all the vacations that are in between a date range
     * 
     * @param startDate {@link LocalDate} that sets the start of the interval.
     *            It's inclusive
     * @param endDate {@link LocalDate} that sets the end of the interval It's
     *            inclusive
     * @return a {@link List} of vacations
     * @throws ApiException if there is an error performing the operation
     */
    List<Vacation> getVacationsBetween(LocalDate startDate, LocalDate endDate) throws ApiException;

    /**
     * Saves or modify the given vacation
     * 
     * @param vacation the vacation
     * @return the saved vacation
     * @throws ApiException if there is an error performing the operation
     */
    Vacation saveVacation(Vacation vacation) throws ApiException;

    /**
     * Delete a vacation from the application. This is a physical operation, so
     * it can't be undone.
     * 
     * @param vacation the vacation you want to delete
     * @return the deleted vacation
     * @throws ApiException if there is an error performing the operation
     */
    Vacation deleteVacation(Vacation vacation) throws ApiException;
}
