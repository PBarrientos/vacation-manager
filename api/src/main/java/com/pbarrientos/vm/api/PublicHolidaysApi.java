
package com.pbarrientos.vm.api;

import java.util.List;

import com.pbarrientos.vm.data.model.PublicHoliday;
import com.pbarrientos.vm.exception.ApiException;

/**
 * Public API that manages all the public holidays in the application
 *
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public interface PublicHolidaysApi {

    /**
     * Method that counts all the existing public holidays
     * 
     * @return the number of public holidays
     * @throws ApiException if there is an error performing the operation
     */
    Long countPublicHolidays() throws ApiException;

    /**
     * Fetch a public holiday by its ID
     * 
     * @param id the id of the public holiday
     * @return the public holiday
     * @throws ApiException if there is an error performing the operation
     */
    PublicHoliday getPublicHoliday(Long id) throws ApiException;

    /**
     * Get a {@link List} with all the existing public holidays
     * 
     * @return the public holidays
     * @throws ApiException if there is an error performing the operation
     */
    List<PublicHoliday> getAllPublicHolidays() throws ApiException;

    /**
     * Fetches all the public holidays within one year
     * 
     * @param year the year
     * @return a {@link List} with the public holidays
     * @throws ApiException if there is an error performing the operation
     */
    List<PublicHoliday> getPublicHolidaysByYear(Integer year) throws ApiException;

    /**
     * Saves a new public holiday
     * 
     * @param publicHoliday the public holiday you want to save
     * @return the saved public holiday
     * @throws ApiException if there is an error performing the operation
     */
    PublicHoliday savePublicHoliday(PublicHoliday publicHoliday) throws ApiException;

    /**
     * Deletes a public holiday from the database. Note that this is a physical
     * deletion, not a logical one. This can't be undone.
     * 
     * @param publicHoliday the public holiday you want to delete
     * @return the deleted public holiday, if the operation cannot be performed
     * @throws ApiException if there is an error performing the operation
     */
    PublicHoliday deletePublicHoliday(PublicHoliday publicHoliday) throws ApiException;

}
