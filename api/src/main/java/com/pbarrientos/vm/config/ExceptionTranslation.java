
package com.pbarrientos.vm.config;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.pbarrientos.vm.data.services.BaseService;
import com.pbarrientos.vm.exception.ServiceException;

/**
 * Aspect that translates all of the possible exceptions thrown by the services
 * into a {@link ServiceException}
 * 
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Order(1)
@Aspect
@Component
public class ExceptionTranslation {

    private final Logger log = Logger.getLogger(ExceptionTranslation.class);

    /**
     * Method that wraps all the {@link BaseService} methods to force them throw
     * a {@link ServiceException} in case something goes wrong
     * 
     * @param pjp the link to the executed method
     * @return the result of the method
     * @throws ServiceException in case anything went wrong
     */
    @Around("execution(* com.pbarrientos.vm.data.services..*(..))")
    public Object translateServiceExceptions(final ProceedingJoinPoint pjp) throws ServiceException {
        try {
            this.log.debug("Executing with: " + pjp.getSignature());
            return pjp.proceed();
        } catch (final Throwable e) {
            final Integer nArgs = pjp.getArgs().length;
            final StringBuilder args = new StringBuilder("");
            if (nArgs > 0) {
                args.append(pjp.getArgs()[0]);
                for (int i = 1; i < nArgs; i++) {
                    args.append(", ").append(pjp.getArgs()[i]);
                }
            }
            final String message =
                    String.format("[ERROR - %s] %s#%s(%s) - %s ", LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
                        pjp.getSignature().getDeclaringType(), pjp.getSignature().getName(), args, e.getLocalizedMessage());
            this.log.error(message);
            throw new ServiceException(e);
        }
    }

}
