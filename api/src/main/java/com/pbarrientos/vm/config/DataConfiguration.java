
package com.pbarrientos.vm.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Configuration class for the database and JPA
 * 
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Configuration
@EntityScan("com.pbarrientos.vm.data.model")
@EnableJpaRepositories("com.pbarrientos.vm.data.repository")
@EnableTransactionManagement
public class DataConfiguration {

}
