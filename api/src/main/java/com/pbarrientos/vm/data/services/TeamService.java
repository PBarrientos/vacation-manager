
package com.pbarrientos.vm.data.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pbarrientos.vm.data.model.Team;
import com.pbarrientos.vm.data.repository.BaseRepository;
import com.pbarrientos.vm.data.repository.TeamRepository;
import com.pbarrientos.vm.data.repository.UserRepository;
import com.pbarrientos.vm.exception.ServiceException;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Service
public class TeamService extends BaseService<Team, Long> {

    @Autowired
    private TeamRepository repository;

    @Autowired
    private UserRepository userRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public BaseRepository<Team, Long> getRepository() {
        return repository;
    }

    public Team findByName(final String name) throws ServiceException {
        return repository.findByName(name);
    }

    public Team findByUser(final Long userId) throws ServiceException {
        return userRepository.findTeamByUser(userId);
    }

}
