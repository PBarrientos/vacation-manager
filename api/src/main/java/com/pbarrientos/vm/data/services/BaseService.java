
package com.pbarrientos.vm.data.services;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import com.pbarrientos.vm.data.model.BaseEntity;
import com.pbarrientos.vm.data.repository.BaseRepository;
import com.pbarrientos.vm.exception.ServiceException;

/**
 * Base class that supports in a generic way all of the CRUD operations
 * 
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Transactional
public abstract class BaseService<T extends BaseEntity, I extends Serializable> {

    public abstract BaseRepository<T, I> getRepository();

    /**
     * Returns all instances of the type.
     * 
     * @return all entities
     * @throws ServiceException in case something went wrong
     */
    public List<T> findAll() throws ServiceException {
        return getRepository().findAll();
    }

    /**
     * Returns the number of entities available.
     * 
     * @return the number of entities
     * @throws ServiceException in case something went wrong
     */
    public long count() throws ServiceException {
        return getRepository().count();
    }

    /**
     * Deletes the entity with the given id.
     * 
     * @param id must not be {@literal null}.
     * @throws IllegalArgumentException in case the given {@code id} is
     *             {@literal null}
     * @throws ServiceException in case something went wrong
     */
    public void delete(final I id) throws ServiceException {
        getRepository().delete(id);
    }

    /**
     * Deletes a given entity.
     * 
     * @param entity Entidad que se quiere eliminar de la base de datos
     * @throws IllegalArgumentException in case the given entity is
     *             {@literal null}.
     * @throws ServiceException in case something went wrong
     */
    public void delete(final T entity) throws ServiceException {
        getRepository().delete(entity);
    }

    /**
     * Deletes all entities managed by the repository.
     * 
     * @throws ServiceException in case something went wrong
     */
    public void deleteAll() throws ServiceException {
        getRepository().deleteAll();
    }

    /**
     * Retrieves an entity by its id.
     * 
     * @param id must not be {@literal null}.
     * @return the entity with the given id or {@literal null} if none found
     * @throws IllegalArgumentException if {@code id} is {@literal null}
     * @throws ServiceException in case something went wrong
     */
    public T findOne(final I id) throws ServiceException {
        return getRepository().findOne(id);
    }

    /**
     * Returns whether an entity with the given id exists.
     * 
     * @param id must not be {@literal null}.
     * @return true if an entity with the given id exists, {@literal false}
     *         otherwise
     * @throws IllegalArgumentException if {@code id} is {@literal null}
     * @throws ServiceException in case something went wrong
     */
    public boolean exists(final I id) throws ServiceException {
        return getRepository().exists(id);
    }

    /**
     * Saves a given entity. Use the returned instance for further operations as
     * the save operation might have changed the entity instance completely.
     * 
     * @param entity Entidad que se quiere guardar en base de datos
     * @return the saved entity
     * @throws ServiceException in case something went wrong
     */
    public <S extends T> S save(final S entity) throws ServiceException {
        entity.setLastUpdated(LocalDateTime.now());
        return getRepository().save(entity);
    }

    /**
     * Flushes all pending changes to the database.
     * 
     * @throws ServiceException in case something went wrong
     */
    public void flush() throws ServiceException {
        getRepository().flush();
    }

    /**
     * Saves an entity and flushes changes instantly.
     * 
     * @param entity Entidad que se quiere guardar
     * @return the saved entity
     * @throws ServiceException in case something went wrong
     */
    public <S extends T> S saveAndFlush(final S entity) throws ServiceException {
        entity.setLastUpdated(LocalDateTime.now());
        return getRepository().saveAndFlush(entity);
    }

}
