
package com.pbarrientos.vm.data.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pbarrientos.vm.data.model.User;
import com.pbarrientos.vm.data.repository.BaseRepository;
import com.pbarrientos.vm.data.repository.UserRepository;
import com.pbarrientos.vm.exception.ServiceException;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Service
public class UserService extends BaseService<User, Long> {

    @Autowired
    private UserRepository repository;

    /**
     * {@inheritDoc}
     */
    @Override
    public BaseRepository<User, Long> getRepository() {
        return repository;
    }

    public User findByIdentifier(final String identifier) throws ServiceException {
        return repository.findByIdentifier(identifier);
    }

    public List<User> findUsersInTeam(final Long teamId) throws ServiceException {
        return repository.findByTeamId(teamId);
    }

}
