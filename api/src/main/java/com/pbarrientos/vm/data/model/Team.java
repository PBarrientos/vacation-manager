
package com.pbarrientos.vm.data.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * Team model class
 *
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Entity
@Table(name = "T_VM_M_TEAMS")
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
public class Team extends BaseEntity {

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(name = "PHOTO_URL")
    private String photoUrl;

    @Column(length = 2)
    private String avatar;

    @OneToMany(mappedBy = "team", fetch = FetchType.EAGER)
    @JsonManagedReference
    private List<User> members;

    public Team() {
        this.members = new ArrayList<>();
    }

    public void addMember(final User user) {
        if (!members.contains(user)) {
            this.members.add(user);
            user.setTeam(this);
        }
    }

    public void removeMember(final User user) {
        user.setTeam(null);
        this.members.remove(user);
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(final String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(final List<User> members) {
        this.members = members;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(final String avatar) {
        this.avatar = avatar;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(final Object other) {
        boolean result = false;

        if (this == other) {
            result = true;
        } else if (other == null) {
            result = false;
        } else if (other instanceof Long) {
            result = getId().equals(other);
        } else if (getId() != null) {
            result = getId().equals(((Team) other).getId());
        }

        return result;
    }

}
