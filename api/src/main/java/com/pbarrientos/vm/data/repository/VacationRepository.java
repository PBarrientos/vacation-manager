
package com.pbarrientos.vm.data.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pbarrientos.vm.data.model.Vacation;

/**
 * JPA repository for {@link Vacation}
 * 
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Repository
public interface VacationRepository extends BaseRepository<Vacation, Long> {

    List<Vacation> findByUserId(Long userId);

    List<Vacation> findByUserTeamId(Long teamId);

    @Query(value = "select v from Vacation v where v.startDate <= ?2 and v.endDate >= ?1")
    List<Vacation> findBetween(LocalDate start, LocalDate end);

}
