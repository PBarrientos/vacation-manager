
package com.pbarrientos.vm.data.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pbarrientos.vm.data.model.PublicHoliday;
import com.pbarrientos.vm.data.repository.BaseRepository;
import com.pbarrientos.vm.data.repository.PublicHolidayRepository;
import com.pbarrientos.vm.exception.ServiceException;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Service
public class PublicHolidayService extends BaseService<PublicHoliday, Long> {

    @Autowired
    private PublicHolidayRepository repository;

    /**
     * {@inheritDoc}
     */
    @Override
    public BaseRepository<PublicHoliday, Long> getRepository() {
        return repository;
    }

    public List<PublicHoliday> findByYear(final Integer year) throws ServiceException {
        return repository.findByYear(year);
    }

}
