
package com.pbarrientos.vm.data.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 * Abstract class that supports all the remaining entities for provide common
 * functionality
 * 
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Id of the entity
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    /**
     * Version number of the entity
     */
    @Version
    protected Long version;

    /**
     * Last time the entity was updated
     */
    @Column(nullable = false, name = "LAST_UPDATED")
    protected LocalDateTime lastUpdated;

    // Equals and Hashcode ----------------------------------------------------
    @Override
    public int hashCode() {
        return getClass().hashCode() + getId().intValue();
    }

    @Override
    public String toString() {
        return String.format("%s[id=%d , version=%d]", getClass().getSimpleName(), this.id, this.version);
    }

    @Override
    public boolean equals(final Object other) {
        boolean result = false;

        if (this == other) {
            result = true;
        } else if (other == null) {
            result = false;
        } else if (other instanceof Long) {
            result = getId().equals(other);
        }

        return result;
    }

    // Getters and setters -----------------------------------------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(final Long version) {
        this.version = version;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(final LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

}
