
package com.pbarrientos.vm.data.enums;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public enum EnumRoles {

    ADMINISTRATOR(1L, "ADMINISTRATOR", "role.administrator"),
    USER(2L, "USER", "role.user");

    // Attributes -------------------------------------------------------------

    private final Long id;

    private final String code;

    private final String description;

    EnumRoles(final Long id, final String code, final String description) {
        this.id = id;
        this.code = code;
        this.description = description;
    }

    // Getters ----------------------------------------------------------------

    /**
     * @return el atributo id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @return el atributo codigo
     */
    public String getCode() {
        return this.code;
    }

    /**
     * @return el atributo descripcion
     */
    public String getDescription() {
        return this.description;
    }

}
