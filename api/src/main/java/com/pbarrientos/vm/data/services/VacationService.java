
package com.pbarrientos.vm.data.services;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pbarrientos.vm.data.model.User;
import com.pbarrientos.vm.data.model.Vacation;
import com.pbarrientos.vm.data.repository.BaseRepository;
import com.pbarrientos.vm.data.repository.UserRepository;
import com.pbarrientos.vm.data.repository.VacationRepository;
import com.pbarrientos.vm.exception.ServiceException;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Service
public class VacationService extends BaseService<Vacation, Long> {

    @Autowired
    private VacationRepository repository;

    @Autowired
    private UserRepository userRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public BaseRepository<Vacation, Long> getRepository() {
        return repository;
    }

    public Vacation createVacation(final Vacation vacation) throws ServiceException {
        vacation.setApproved(false);
        vacation.setManaged(false);
        return save(vacation);
    }

    public List<Vacation> findByUser(final Long userId) throws ServiceException {
        return repository.findByUserId(userId);

    }

    public List<Vacation> findByTeam(final Long teamId) throws ServiceException {
        return repository.findByUserTeamId(teamId);
    }

    public List<Vacation> findBetween(final LocalDate start, final LocalDate end) throws ServiceException {
        return repository.findBetween(start, end);
    }

    @Override
    public void delete(final Vacation vacation) throws ServiceException {
        final User user = vacation.getUser();

        user.removeVacation(vacation);
        userRepository.saveAndFlush(user);
    }

}
