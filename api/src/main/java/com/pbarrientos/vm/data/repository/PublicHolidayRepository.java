
package com.pbarrientos.vm.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pbarrientos.vm.data.model.PublicHoliday;

/**
 * JPA repository for {@link PublicHoliday}
 * 
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Repository
public interface PublicHolidayRepository extends BaseRepository<PublicHoliday, Long> {

    @Query(value = "select ph FROM PublicHoliday ph where YEAR(ph.day) = ?1")
    List<PublicHoliday> findByYear(Integer year);

}
