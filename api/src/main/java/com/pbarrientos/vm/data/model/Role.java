
package com.pbarrientos.vm.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.pbarrientos.vm.data.enums.EnumRoles;

/**
 * Role model class
 *
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Entity
@Table(name = "T_VM_P_ROLES")
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
public class Role implements GrantedAuthority {

    /**
     * Serial Version UID -----------------------------------------------------
     */
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column(unique = true, updatable = false, length = 50)
    private String code;

    @Column(length = 500)
    private String description;

    /**
     * Default constructor
     */
    public Role() {

    }

    /**
     * Constructor that initializes the data with the enum
     *
     * @param role the role we want to initialize
     */
    public Role(final EnumRoles role) {
        this.id = role.getId();
        this.code = role.getCode();
        this.description = role.getDescription();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAuthority() {
        return this.code;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    // Equals and Hashcode ----------------------------------------------------
    @Override
    public int hashCode() {
        return getClass().hashCode() + getId().intValue();
    }

    @Override
    public String toString() {
        return String.format("%s[id=%d , code=%s]", getClass().getSimpleName(), this.id, this.code);
    }

    @Override
    public boolean equals(final Object obj) {
        return (obj instanceof EnumRoles) ? id.equals(((EnumRoles) obj).getId()) : super.equals(obj);
    }

}
