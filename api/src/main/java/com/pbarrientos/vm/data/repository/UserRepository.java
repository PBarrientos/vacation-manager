
package com.pbarrientos.vm.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pbarrientos.vm.data.model.Team;
import com.pbarrientos.vm.data.model.User;

/**
 * JPA repository for {@link User}
 * 
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Repository
public interface UserRepository extends BaseRepository<User, Long> {

    User findByIdentifier(String identifier);

    List<User> findByTeamId(Long teamId);

    @Query(value = "select u.team from User u where u.id = ?1")
    Team findTeamByUser(Long userId);
}
