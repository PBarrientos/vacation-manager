
package com.pbarrientos.vm.data.repository;

import org.springframework.stereotype.Repository;

import com.pbarrientos.vm.data.model.Role;

/**
 * JPA repository for {@link Role}
 * 
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Repository
public interface RoleRepository extends BaseRepository<Role, Long> {

}
