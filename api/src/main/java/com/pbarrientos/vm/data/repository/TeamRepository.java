
package com.pbarrientos.vm.data.repository;

import org.springframework.stereotype.Repository;

import com.pbarrientos.vm.data.model.Team;

/**
 * JPA repository for {@link Team}
 * 
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Repository
public interface TeamRepository extends BaseRepository<Team, Long> {

    Team findByName(String name);

}
