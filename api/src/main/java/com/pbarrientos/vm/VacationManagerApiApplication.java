
package com.pbarrientos.vm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main class of the application.
 *
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@SpringBootApplication
public class VacationManagerApiApplication {

    public static void main(final String[] args) {
        SpringApplication.run(VacationManagerApiApplication.class, args);
    }
}
