
package com.pbarrientos.vm.exception;

/**
 * Exception throw by the Service methods
 * 
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public class ServiceException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * {@inheritDoc}
     */
    public ServiceException() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    public ServiceException(final String message) {
        super(message);
    }

    /**
     * {@inheritDoc}
     */
    public ServiceException(final Throwable cause) {
        super(cause);
    }

    /**
     * {@inheritDoc}
     */
    public ServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * {@inheritDoc}
     */
    public ServiceException(final String message,
        final Throwable cause,
        final boolean enableSuppression,
        final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
