
package com.pbarrientos.vm.exception;

/**
 * Exception thrown by the API
 *
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public class ApiException extends Exception {

    private static final long serialVersionUID = 1L;

    public ApiException() {
        super();
    }

    public ApiException(final String arg0) {
        super(arg0);
    }

    public ApiException(final Throwable cause) {
        super(cause);
    }

    public ApiException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ApiException(final String message,
        final Throwable cause,
        final boolean enableSuppression,
        final boolean writableStackTrace) {

        super(message, cause, enableSuppression, writableStackTrace);
    }

}
