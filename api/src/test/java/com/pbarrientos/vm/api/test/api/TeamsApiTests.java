
package com.pbarrientos.vm.api.test.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.pbarrientos.vm.api.TeamsApi;
import com.pbarrientos.vm.api.test.base.DataUnitTest;
import com.pbarrientos.vm.data.model.Team;
import com.pbarrientos.vm.exception.ApiException;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public class TeamsApiTests extends DataUnitTest {

    @Autowired
    private TeamsApi api;

    @Test(priority = 5)
    public void getTeamById() throws ApiException {
        final Team team = api.getTeam(1l);
        Assert.assertNotNull(team);
        Assert.assertTrue(team.getId().equals(1l));
    }

    @Test(priority = 5)
    public void getTeamByName() throws ApiException {
        final Team team = api.getTeam("Crossfield");
        Assert.assertNotNull(team);
        Assert.assertTrue(team.getId().equals(1l));
    }

    @Test(priority = 5)
    public void getAllTeams() throws ApiException {
        final List<Team> allTeams = api.getAllTeams();
        Assert.assertNotNull(allTeams);
        Assert.assertTrue(allTeams.size() == 1);
    }

    @Test(priority = 5)
    public void getTeamByUser() throws ApiException {
        final Team team = api.getTeamByUser(2l);
        Assert.assertNotNull(team);
        Assert.assertTrue(team.getName().equals("Crossfield"));
    }

    @Test(priority = 6)
    public void saveTeam() throws ApiException {
        final Team team = new Team();
        team.setName("Test team");
        team.setAvatar("TT");

        final long countBefore = api.countTeams();

        api.saveTeam(team);

        final long countAfter = api.countTeams();

        Assert.assertTrue(countAfter == countBefore + 1);
    }

    @Test(priority = 7)
    public void deleteTeam() throws ApiException {
        final Team team = api.getTeam("Test team");

        api.deleteTeam(team);

        final Team afterDeleteTeam = api.getTeam("Test team");

        Assert.assertNull(afterDeleteTeam);

    }

}
