
package com.pbarrientos.vm.api.test.api;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.pbarrientos.vm.api.UsersApi;
import com.pbarrientos.vm.api.test.base.DataUnitTest;
import com.pbarrientos.vm.data.enums.EnumRoles;
import com.pbarrientos.vm.data.model.Role;
import com.pbarrientos.vm.data.model.User;
import com.pbarrientos.vm.exception.ApiException;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public class UsersApiTests extends DataUnitTest {

    @Autowired
    private UsersApi api;

    @Test(priority = 1)
    public void getUserById() throws ApiException {
        final User user = api.getUser(2l);
        Assert.assertNotNull(user);
        Assert.assertTrue(user.getId().equals(2l));
    }

    @Test(priority = 1)
    public void getUserByName() throws ApiException {
        final User user = api.getUser("pb");
        Assert.assertNotNull(user);
        Assert.assertTrue(user.getId().equals(2l));
    }

    @Test(priority = 1)
    public void getAllUsers() throws ApiException {
        final List<User> allUsers = api.getAllUsers();
        Assert.assertNotNull(allUsers);
        Assert.assertTrue(allUsers.size() == 2);
    }

    @Test(priority = 1)
    public void getAllUsersByTeam() throws ApiException {
        final List<User> allUsers = api.getAllUsersByTeam(1l);
        Assert.assertNotNull(allUsers);
        Assert.assertTrue(allUsers.size() == 2);
    }

    @Test(priority = 2)
    public void saveUser() throws ApiException {

        final Role role = new Role(EnumRoles.USER);
        final User user = new User();
        user.setIdentifier("Test1");
        user.setPassword("password");
        user.setName("Test");
        user.setSurname("Test");
        user.setBirthdate(LocalDate.now());
        user.setIsEnabled(true);
        user.setLocked(false);
        user.setAttempts(0);
        user.setRole(role);
        user.setLastUpdated(LocalDateTime.now());

        final long countBefore = api.countUsers();

        api.saveUser(user);

        final long countAfter = api.countUsers();

        Assert.assertTrue(countAfter == countBefore + 1);
    }

    @Test(priority = 3)
    public void changePassword() throws ApiException {
        final User user = api.getUser("Test1");
        final User userAfter = api.changePassword(user, "newPassword");

        Assert.assertFalse(userAfter.getPassword().equals("$2a$05$HPWod9nB7njAsiclo5KswenuQ9JxvboOjZOxztqvLPOc7wphp04nq"));
    }

    @Test(priority = 4)
    public void deleteUser() throws ApiException {
        final User user = api.getUser("Test1");
        api.deleteUser(user);

        final User userAfter = api.getUser("Test1");
        Assert.assertNull(userAfter);
    }

}
