
package com.pbarrientos.vm.api.test.api;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.pbarrientos.vm.api.PublicHolidaysApi;
import com.pbarrientos.vm.api.test.base.DataUnitTest;
import com.pbarrientos.vm.data.model.PublicHoliday;
import com.pbarrientos.vm.exception.ApiException;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public class PublicHolidaysApiTests extends DataUnitTest {

    @Autowired
    private PublicHolidaysApi api;

    @Test(priority = 8)
    public void getPublicHolidayById() throws ApiException {
        final PublicHoliday publicHoliday = api.getPublicHoliday(1l);
        Assert.assertNotNull(publicHoliday);
        Assert.assertTrue(publicHoliday.getDescription().equals("New Year"));
    }

    @Test(priority = 8)
    public void getPublicHolidayByYear() throws ApiException {
        final List<PublicHoliday> publicHoliday = api.getPublicHolidaysByYear(2017);
        Assert.assertNotNull(publicHoliday);
        Assert.assertTrue(publicHoliday.size() == 1);
    }

    @Test(priority = 8)
    public void getAllPublicHolidays() throws ApiException {
        final List<PublicHoliday> publicHoliday = api.getAllPublicHolidays();
        Assert.assertNotNull(publicHoliday);
        Assert.assertTrue(publicHoliday.size() == 1);
    }

    @Test(priority = 9)
    public void savePublicHoliday() throws ApiException {

        final PublicHoliday publicHoliday = new PublicHoliday();
        publicHoliday.setDay(LocalDate.of(2018, 2, 28));
        publicHoliday.setDescription("Andalucia's day");

        final long countBefore = api.countPublicHolidays();

        api.savePublicHoliday(publicHoliday);

        final long countAfter = api.countPublicHolidays();

        Assert.assertTrue(countAfter == countBefore + 1);
    }

    @Test(priority = 10)
    public void deletePublicHoliday() throws ApiException {
        final List<PublicHoliday> publicHolidays = api.getPublicHolidaysByYear(2018);

        Assert.assertTrue(publicHolidays.size() == 1);

        api.deletePublicHoliday(publicHolidays.get(0));

        final List<PublicHoliday> publicHolidaysAfter = api.getPublicHolidaysByYear(2018);

        Assert.assertTrue(publicHolidaysAfter.size() == 0);

    }

}
