
package com.pbarrientos.vm.api.test.base;

import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version
 * @since
 */
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
public abstract class DataUnitTest extends BaseUnitTest {

}
