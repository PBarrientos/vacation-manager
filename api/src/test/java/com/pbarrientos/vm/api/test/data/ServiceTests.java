
package com.pbarrientos.vm.api.test.data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.pbarrientos.vm.api.test.base.DataUnitTest;
import com.pbarrientos.vm.data.enums.EnumRoles;
import com.pbarrientos.vm.data.model.PublicHoliday;
import com.pbarrientos.vm.data.model.Role;
import com.pbarrientos.vm.data.model.Team;
import com.pbarrientos.vm.data.model.User;
import com.pbarrientos.vm.data.model.Vacation;
import com.pbarrientos.vm.data.services.PublicHolidayService;
import com.pbarrientos.vm.data.services.TeamService;
import com.pbarrientos.vm.data.services.UserService;
import com.pbarrientos.vm.data.services.VacationService;
import com.pbarrientos.vm.exception.ServiceException;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public class ServiceTests extends DataUnitTest {

    @Autowired
    private UserService userService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private PublicHolidayService publicHolidaysService;

    @Autowired
    private VacationService vacationService;

    @Test
    public void contextLoads() {

    }

    /**
     * Test that the repository count the real users in the database;
     * 
     * @throws ServiceException
     */
    @Test(groups = "readServices")
    public void userreadServicesTest() throws ServiceException {
        final List<User> users = userService.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(users.size(), 2);
    }

    @Test(groups = "readServices")
    public void teamsreadServicesTest() throws ServiceException {
        final List<Team> teams = teamService.findAll();
        Assert.assertNotNull(teams);
        Assert.assertEquals(teams.size(), 1);
    }

    @Test(groups = "readServices")
    public void publicHolidaysreadServicesTest() throws ServiceException {
        final List<PublicHoliday> pholidays = publicHolidaysService.findAll();
        Assert.assertNotNull(pholidays);
        Assert.assertEquals(pholidays.size(), 1);
    }

    @Test(groups = "readServices")
    public void vacationreadServicesTest() throws ServiceException {
        final List<Vacation> vacations = vacationService.findAll();
        Assert.assertNotNull(vacations);
        Assert.assertEquals(vacations.size(), 1);
    }

    @Test(groups = "readServices")
    public void findUsersByTeam() throws ServiceException {
        final List<User> users = userService.findUsersInTeam(1l);
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() == 2);
    }

    @Test(groups = "readServices")
    public void findTeamByUser() throws ServiceException {
        final Team team = teamService.findByUser(2l);
        Assert.assertNotNull(team);
        Assert.assertTrue(team.getId().equals(1l));
    }

    @Test(groups = "readServices")
    public void findVacationsByUser() throws ServiceException {
        final List<Vacation> vacations = vacationService.findByUser(2l);
        Assert.assertNotNull(vacations);
        Assert.assertTrue(vacations.size() == 1);
    }

    @Test(groups = "readServices")
    public void findVacationsByTeam() throws ServiceException {
        final List<Vacation> vacations = vacationService.findByTeam(1l);
        Assert.assertNotNull(vacations);
        Assert.assertTrue(vacations.size() == 1);
    }

    @Test(groups = "writeServices", dependsOnGroups = "readServices", alwaysRun = true)
    public void addUser() throws ServiceException {
        final long countBefore = userService.count();

        final Role role = new Role(EnumRoles.USER);
        final User user = new User();
        user.setIdentifier("Test1");
        user.setPassword("password");
        user.setName("Test");
        user.setSurname("Test");
        user.setBirthdate(LocalDate.now());
        user.setIsEnabled(true);
        user.setLocked(false);
        user.setAttempts(0);
        user.setRole(role);
        user.setLastUpdated(LocalDateTime.now());

        userService.saveAndFlush(user);
        final long countAfter = userService.count();

        Assert.assertTrue(countAfter == countBefore + 1);

    }

    @Test(groups = "writeServices", dependsOnGroups = "readServices", alwaysRun = true)
    public void addTeam() throws ServiceException {
        final long countBefore = teamService.count();

        final Team team = new Team();
        team.setName("Team test");
        team.setLastUpdated(LocalDateTime.now());

        teamService.saveAndFlush(team);
        final long countAfter = teamService.count();

        Assert.assertTrue(countAfter == countBefore + 1);

    }

    @Test(groups = "writeServices", dependsOnGroups = "readServices", alwaysRun = true)
    public void addPublicHoliday() throws ServiceException {
        final long countBefore = publicHolidaysService.count();

        final PublicHoliday ph = new PublicHoliday();
        ph.setDay(LocalDate.of(2017, 07, 21));
        ph.setLastUpdated(LocalDateTime.now());

        publicHolidaysService.saveAndFlush(ph);
        final long countAfter = publicHolidaysService.count();

        Assert.assertTrue(countAfter == countBefore + 1);

    }

    @Test(groups = "writeServices", dependsOnGroups = "readServices", alwaysRun = true)
    public void addVacation() throws ServiceException {
        final long countBefore = vacationService.count();

        final User user = userService.findOne(2l);

        Assert.assertNotNull(user);

        final Vacation vacation = new Vacation();
        vacation.setLastUpdated(LocalDateTime.now());
        vacation.setYear(2017);
        vacation.setStartDate(LocalDate.of(2017, 1, 4));
        vacation.setEndDate(LocalDate.of(2017, 1, 7));
        vacation.setApproved(false);
        vacation.setManaged(false);

        user.addVacation(vacation);

        userService.saveAndFlush(user);
        final long countAfter = vacationService.count();

        Assert.assertTrue(countAfter == countBefore + 1);
    }

    /**
     * Check that a failed operation throws a {@link ServiceException}
     * 
     * @throws ServiceException
     */
    @Test(groups = "writeServices", dependsOnGroups = "readServices", alwaysRun = true, expectedExceptions = ServiceException.class)
    public void badUser() throws ServiceException {
        final User user = userService.findOne(2l);
        user.setId(null);
        user.setVersion(null);

        userService.save(user);
    }

    @Test(groups = "deleteServices", dependsOnGroups = "writeServices")
    public void deleteUser() throws ServiceException {
        final User userBefore = userService.findByIdentifier("Test1");
        Assert.assertNotNull(userBefore);

        userService.delete(userBefore);

        final User userAfter = userService.findByIdentifier("Test1");
        Assert.assertNull(userAfter);

    }

    @Test(groups = "deleteServices", dependsOnGroups = "writeServices")
    public void deletePublicHoliday() throws ServiceException {
        final long countBefore = publicHolidaysService.count();

        publicHolidaysService.delete(1l);

        final long countAfter = publicHolidaysService.count();
        Assert.assertTrue(countAfter == countBefore - 1);
    }

    @Test(groups = "deleteServices", dependsOnGroups = "writeServices", dependsOnMethods = "deleteUser")
    public void deleteAllUsers() throws ServiceException {
        userService.deleteAll();
        userService.flush();
        final long count = userService.count();
        Assert.assertTrue(count == 0);
    }

}
