
package com.pbarrientos.vm.api.test.base;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

/**
 * Base class for all the unit tests
 * 
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@SpringBootTest
public abstract class BaseUnitTest extends AbstractTestNGSpringContextTests {

}
