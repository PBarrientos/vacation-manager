
package com.pbarrientos.vm.api.test.api;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.pbarrientos.vm.api.UsersApi;
import com.pbarrientos.vm.api.VacationApi;
import com.pbarrientos.vm.api.test.base.DataUnitTest;
import com.pbarrientos.vm.data.model.User;
import com.pbarrientos.vm.data.model.Vacation;
import com.pbarrientos.vm.exception.ApiException;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public class VacationsApiTests extends DataUnitTest {

    @Autowired
    private VacationApi api;

    @Autowired
    private UsersApi userApi;

    @Test(priority = 11)
    public void getVacationById() throws ApiException {
        final Vacation vacation = api.getVacation(1l);
        Assert.assertNotNull(vacation);
        Assert.assertTrue(vacation.getId().equals(1l));
    }

    @Test(priority = 11)
    public void getAllVacations() throws ApiException {
        final List<Vacation> allVacations = api.getAllVacations();
        Assert.assertNotNull(allVacations);
        Assert.assertTrue(allVacations.size() == 1);
    }

    @Test(priority = 11)
    public void getVacationsByUser() throws ApiException {
        final List<Vacation> vacations = api.getVacationsByUser(2l);
        Assert.assertNotNull(vacations);
        Assert.assertTrue(vacations.size() == 1);
    }

    @Test(priority = 11)
    public void getVacationsByTeam() throws ApiException {
        final List<Vacation> vacations = api.getVacationsByTeam(1l);
        Assert.assertNotNull(vacations);
        Assert.assertTrue(vacations.size() == 1);
    }

    @Test(priority = 11)
    public void getVacationsBetween() throws ApiException {
        final List<Vacation> vacations = api.getVacationsBetween(LocalDate.of(2017, 01, 04), LocalDate.of(2017, 01, 10));
        Assert.assertNotNull(vacations);
        Assert.assertTrue(vacations.size() == 1);
    }

    @Test(priority = 12)
    public void saveVacation() throws ApiException {
        final Vacation vacation = new Vacation();
        final User user = userApi.getUser(2l);
        vacation.setUser(user);
        vacation.setStartDate(LocalDate.of(2017, 2, 1));
        vacation.setEndDate(LocalDate.of(2017, 2, 5));
        vacation.setYear(2017);

        final long countBefore = api.countVacations();

        api.saveVacation(vacation);

        final long countAfter = api.countVacations();

        Assert.assertTrue(countAfter == countBefore + 1);
    }

    @Test(priority = 13)
    public void deleteVacation() throws ApiException {
        final List<Vacation> vacations = api.getVacationsBetween(LocalDate.of(2017, 2, 1), LocalDate.of(2017, 2, 5));

        Assert.assertNotNull(vacations);
        Assert.assertTrue(vacations.size() == 1);

        final Vacation v = vacations.get(0);

        api.deleteVacation(v);

        final List<Vacation> vacationsAfter = api.getVacationsBetween(LocalDate.of(2017, 2, 1), LocalDate.of(2017, 2, 5));

        Assert.assertNotNull(vacationsAfter);
        Assert.assertTrue(vacationsAfter.size() == 0);

    }

}
