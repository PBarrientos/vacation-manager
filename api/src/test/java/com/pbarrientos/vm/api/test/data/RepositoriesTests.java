
package com.pbarrientos.vm.api.test.data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.pbarrientos.vm.api.test.base.DataUnitTest;
import com.pbarrientos.vm.data.enums.EnumRoles;
import com.pbarrientos.vm.data.model.PublicHoliday;
import com.pbarrientos.vm.data.model.Role;
import com.pbarrientos.vm.data.model.Team;
import com.pbarrientos.vm.data.model.User;
import com.pbarrientos.vm.data.model.Vacation;
import com.pbarrientos.vm.data.repository.PublicHolidayRepository;
import com.pbarrientos.vm.data.repository.TeamRepository;
import com.pbarrientos.vm.data.repository.UserRepository;
import com.pbarrientos.vm.data.repository.VacationRepository;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public class RepositoriesTests extends DataUnitTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private PublicHolidayRepository publicHolidaysRepository;

    @Autowired
    private VacationRepository vacationRepository;

    @Test
    public void contextLoads() {

    }

    /**
     * Test that the repository count the real users in the database;
     */
    @Test(groups = "readRepositories")
    public void userReadTest() {
        final List<User> users = userRepository.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(users.size(), 2);
    }

    @Test(groups = "readRepositories")
    public void teamsReadTest() {
        final List<Team> teams = teamRepository.findAll();
        Assert.assertNotNull(teams);
        Assert.assertEquals(teams.size(), 1);
    }

    @Test(groups = "readRepositories")
    public void publicHolidaysReadTest() {
        final List<PublicHoliday> pholidays = publicHolidaysRepository.findAll();
        Assert.assertNotNull(pholidays);
        Assert.assertEquals(pholidays.size(), 1);
    }

    @Test(groups = "readRepositories")
    public void vacationReadTest() {
        final List<Vacation> vacations = vacationRepository.findAll();
        Assert.assertNotNull(vacations);
        Assert.assertEquals(vacations.size(), 1);
    }

    @Test(groups = "readRepositories")
    public void findUsersByTeam() {
        final List<User> users = userRepository.findByTeamId(1l);
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() == 2);
    }

    @Test(groups = "readRepositories")
    public void findTeamByUser() {
        final Team team = userRepository.findTeamByUser(2l);
        Assert.assertNotNull(team);
        Assert.assertTrue(team.getId().equals(1l));
    }

    @Test(groups = "readRepositories")
    public void findVacationsByUser() {
        final List<Vacation> vacations = vacationRepository.findByUserId(2l);
        Assert.assertNotNull(vacations);
        Assert.assertTrue(vacations.size() == 1);
    }

    @Test(groups = "readRepositories")
    public void findVacationsByTeam() {
        final List<Vacation> vacations = vacationRepository.findByUserTeamId(1l);
        Assert.assertNotNull(vacations);
        Assert.assertTrue(vacations.size() == 1);
    }

    @Test(groups = "readRepositories")
    public void findVacationsBetween() {
        final List<Vacation> vacations = vacationRepository.findBetween(LocalDate.of(2017, 01, 04), LocalDate.of(2017, 01, 15));
        Assert.assertNotNull(vacations);
        Assert.assertTrue(vacations.size() == 1);
    }

    @Test(groups = "readRepositories")
    public void findVacationsBetweenNull() {
        final List<Vacation> vacations = vacationRepository.findBetween(LocalDate.of(2017, 02, 04), LocalDate.of(2017, 02, 15));
        Assert.assertNotNull(vacations);
        Assert.assertTrue(vacations.size() == 0);
    }

    @Test(groups = "writeRepositories", dependsOnGroups = "readRepositories", alwaysRun = true)
    public void addUser() {
        final long countBefore = userRepository.count();

        final Role role = new Role(EnumRoles.USER);
        final User user = new User();
        user.setIdentifier("Test1");
        user.setPassword("password");
        user.setName("Test");
        user.setSurname("Test");
        user.setBirthdate(LocalDate.now());
        user.setIsEnabled(true);
        user.setLocked(false);
        user.setAttempts(0);
        user.setRole(role);
        user.setLastUpdated(LocalDateTime.now());

        userRepository.saveAndFlush(user);
        final long countAfter = userRepository.count();

        Assert.assertTrue(countAfter == countBefore + 1);

    }

    @Test(groups = "writeRepositories", dependsOnGroups = "readRepositories", alwaysRun = true)
    public void addTeam() {
        final long countBefore = teamRepository.count();

        final Team team = new Team();
        team.setName("Team test");
        team.setLastUpdated(LocalDateTime.now());

        teamRepository.saveAndFlush(team);
        final long countAfter = teamRepository.count();

        Assert.assertTrue(countAfter == countBefore + 1);

    }

    @Test(groups = "writeRepositories", dependsOnGroups = "readRepositories", alwaysRun = true)
    public void addPublicHoliday() {
        final long countBefore = publicHolidaysRepository.count();

        final PublicHoliday ph = new PublicHoliday();
        ph.setDay(LocalDate.of(2017, 07, 21));
        ph.setLastUpdated(LocalDateTime.now());

        publicHolidaysRepository.saveAndFlush(ph);
        final long countAfter = publicHolidaysRepository.count();

        Assert.assertTrue(countAfter == countBefore + 1);

    }

    @Test(groups = "writeRepositories", dependsOnGroups = "readRepositories", alwaysRun = true)
    public void addVacation() {
        final long countBefore = vacationRepository.count();

        final User user = userRepository.findOne(2l);

        Assert.assertNotNull(user);

        final Vacation vacation = new Vacation();
        vacation.setLastUpdated(LocalDateTime.now());
        vacation.setYear(2017);
        vacation.setStartDate(LocalDate.of(2017, 1, 4));
        vacation.setEndDate(LocalDate.of(2017, 1, 7));
        vacation.setApproved(false);
        vacation.setManaged(false);

        user.addVacation(vacation);

        userRepository.saveAndFlush(user);
        final long countAfter = vacationRepository.count();

        Assert.assertTrue(countAfter == countBefore + 1);
    }
}
