insert into T_VM_P_ROLES values (1, 'ADMIN', 'admin');
insert into T_VM_P_ROLES values (2, 'USER', 'user');

insert into T_VM_M_TEAMS values (1, CURDATE(), 1, 'XF', 'Crossfield', null);

INSERT INTO T_VM_M_USERS (`id`, `last_updated`, `version`, `attempts`, `birthdate`, `city`, `country`, `email`, `identifier`, `is_Enabled`, `locked`, `name`, `password`, `surname`, `vacation_Days`, `role_id`, `TEAM_ID`) VALUES ('1', CURDATE(), '1', '0', '2017-01-01', 'Seville', 'Spain', 'admin@vacation-manager.com', 'admin', true, false, 'Admin', '$2a$05$HPWod9nB7njAsiclo5KswenuQ9JxvboOjZOxztqvLPOc7wphp04nq', 'Admin', '22', '1', '1');
INSERT INTO T_VM_M_USERS (`id`, `last_updated`, `version`, `attempts`, `birthdate`, `city`, `country`, `email`, `identifier`, `is_Enabled`, `locked`, `name`, `password`, `surname`, `vacation_Days`, `role_id`, `TEAM_ID`) VALUES ('2', CURDATE(), '1', '0', '1989-07-21', 'Seville', 'Spain', 'pablo.barrientos.13@gmail.com', 'pb', true, false, 'Pablo', '$2a$05$HPWod9nB7njAsiclo5KswenuQ9JxvboOjZOxztqvLPOc7wphp04nq', 'Barrientos', '22', '2', '1');

insert into T_VM_M_PUBLIC_HOLIDAYS values (1, CURDATE(), 1, '2017-01-01', 'New Year');

insert into T_VM_M_VACATIONS values (1, CURDATE(), 1, false, '2017-01-07', false, '2017-01-10', 2017, 2);