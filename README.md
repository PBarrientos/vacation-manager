# Vacation Manager

The vacation manager is an application for manage the vacations of all the users of an organization that is using agile methodologies, although it can work for other kind of organizations.

It has the possibility to split the users into different teams. These teams have an own calendar, and its users vacations are only visible to its members.

The team can have a manager (usually the product owner or the scrum master), who may have the possibility of accept or reject the pending vacations. This possibility can be turned off if it's needed, so all the vacations are approbed by default.

### Project technologies and architecture

The project is splitted into three different modules:

* **api** : It's a JAR composed by the API methods and the data management
* **rest** : It's a WAR ready to be deployed in an application server. This WAR contains the API and serves as the REST entry point for the API.
* **ui** : It's a WAR that contains the UI files. It's built under @angular

The main technologies used for this project are:
* Java 8
* Spring Boot
* @angular
* Gradle 3.5
* Tomcat 8.5

### Getting started
The project comes with a fully automated gradle script. The most used commands are:
* ```./gradlew clean``` : Clean all the generated code of the project, including /build, /node_modules and compilated Javascript for the project
* ```./gradlew build``` : Builds the project, transpile the typescript into javascript and executes the tests of the application
* ```./gradlew install``` : Executes a build of the application and create a **build/libs** folder into the root project with the distribution files of the project
* ```./gradlew sonarqube``` : Analyses the project with sonarqube. Note that, for executing this command, you will need to have a local Sonarqube instance installed in your computer and running. You can change the URL for your instance in the _```gradle.properties```_ file
* ```./gradlew dockerBuild``` : Create a Docker image with a Tomcat application server and the distribution files included in it
* ```./gradlew dockerRun``` : Run the previously generated docker image.
* ```./gradlew dockerRelease``` : Creates a docker image uploads it to the Gitlab registry. For use this feature, you must set the username and password for Gitlab in a file called ```properties.gradle``` within your local Gradle installation, in the properties ```gitlab.username``` and ```gitlab.password```. You can also set those properties on runtime, with the following syntax: ```./gradlew dockerRelease -Dgitlab.username=username...```. The default version will be ```latest```. For making the build with the current project version, you have to execute the command with ```-Ptag``` argument.


_**NOTE:** For creating a productive build, you must append -Pprod to the gradle command_

_**NOTE:** For using the docker commands a Docker installation must be present in your computer._

#### Docker image
Once you build and run the Docker image, three different context will be available:
* ```/console``` : This is the Tomcat server manager. The username and password are both ```admin```.
* ```/vacation-manager``` : The context path for the web application.
* ```/vm-api``` : The context path for the REST interface.

### Making releases
We are using the Nebula release project.

The following gradle commands are available (be carefu with them):
* ```./gradlew candidate``` : Sets the version to the appropiate <major>.<minor>.<patch>-rc.#, where # is the candidate number
* ```./gradlew devSnapshot``` : This won't create a tag, only will change the version to the appropiate <major>.<minor>.<patch>-dev.#+<hash>
* ```./gradlew final``` : Creates a final version with the appropiate version number
* ```./gradlew snapshot``` : This won't create a tag, but it will set the version to the appropiate snapshot.

You can also bump the major or the patch versions, adding ```-Prelease.scope=<major|patch>```, or set the complete version number, using ```-Prelease.version=<version>```
