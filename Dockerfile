FROM tomcat:8.5

COPY docker/tomcat-config/ /usr/local/tomcat/conf/
COPY build/libs/*rest*.war /usr/local/tomcat/webapps/vm-api.war
COPY build/libs/*ui*.war /usr/local/tomcat/webapps/vm-ui.war