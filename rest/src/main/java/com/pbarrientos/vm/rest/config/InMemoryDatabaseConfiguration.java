
package com.pbarrientos.vm.rest.config;

import org.h2.server.web.WebServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Configuration class for the database used in the development stage
 * 
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@Configuration
@Profile("h2")
public class InMemoryDatabaseConfiguration {

    // Creates a new console for the database management
    @Bean
    ServletRegistrationBean h2ServletRegistrationBean() {
        final ServletRegistrationBean registrationBean = new ServletRegistrationBean(new WebServlet());
        registrationBean.addUrlMappings("/console/*");
        return registrationBean;
    }

}
