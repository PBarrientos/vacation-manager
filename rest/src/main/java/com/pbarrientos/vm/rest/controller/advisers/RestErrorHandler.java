
package com.pbarrientos.vm.rest.controller.advisers;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.pbarrientos.vm.rest.dto.ErrorDto;
import com.pbarrientos.vm.rest.error.VacationManagerException;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@RestControllerAdvice
public class RestErrorHandler {

    private final Logger log = Logger.getLogger(getClass());

    @ExceptionHandler(VacationManagerException.class)
    protected ResponseEntity<ErrorDto> handleRestExceptions(final VacationManagerException e) {
        final ErrorDto error = new ErrorDto();
        error.setCode("400");
        error.setMessage(e.getMessage());

        final ResponseEntity<ErrorDto> result = ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(error);

        return result;
    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<ErrorDto> handleUnknownExceptions(final Exception e, final WebRequest request) {
        // Log the exception
        log.error("Unknow Exception Handled: ", e);

        final ErrorDto error = new ErrorDto();
        error.setCode("500");
        error.setMessage("500: Something went really wrong.");

        final ResponseEntity<ErrorDto> result = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);

        return result;
    }

}
