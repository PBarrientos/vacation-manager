
package com.pbarrientos.vm.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pbarrientos.vm.api.PublicHolidaysApi;
import com.pbarrientos.vm.data.model.PublicHoliday;
import com.pbarrientos.vm.exception.ApiException;
import com.pbarrientos.vm.rest.error.VacationManagerException;
import com.pbarrientos.vm.rest.error.VacationManagerRestErrors;
import com.pbarrientos.vm.rest.utils.DateUtils;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@RestController
@RequestMapping("/public-holidays")
public class PublicHolidaysController {

    private static final VacationManagerRestErrors ERRORS = VacationManagerRestErrors.INSTANCE;

    @Autowired
    private PublicHolidaysApi phApi;

    @GetMapping(value = "/")
    public List<PublicHoliday> getAllPublicHolidays() throws VacationManagerException {
        List<PublicHoliday> result = null;

        try {
            result = phApi.getAllPublicHolidays();
        } catch (final ApiException e) {
            throw ERRORS.allPublicHolidaysException(e);
        }

        return result;
    }

    @PostMapping(value = "/", consumes = "application/json")
    public ResponseEntity<String> addPublicHoliday(@RequestBody final PublicHoliday publicHoliday) throws VacationManagerException {

        //TODO: Validation of the team ?

        try {
            phApi.savePublicHoliday(publicHoliday);
        } catch (final ApiException e) {
            final String date = (publicHoliday != null) ? publicHoliday.getDay().format(DateUtils.LOCAL_DATE) : null;
            throw ERRORS.savePublicHolidayException(date, e);
        }

        return ResponseEntity.ok("The public holiday has been saved without errors");
    }

    @GetMapping(value = "/{publicHolidayId}")
    public PublicHoliday getPublicHoliday(@PathVariable final Long publicHolidayId) throws VacationManagerException {
        PublicHoliday result = null;

        try {
            result = phApi.getPublicHoliday(publicHolidayId);
        } catch (final ApiException e) {
            throw ERRORS.getPublicHolidayByIdException(publicHolidayId, e);
        }

        return result;
    }

    @GetMapping(value = "/year/{year}")
    public List<PublicHoliday> getPublicHolidaysByYear(@PathVariable final Integer year) throws VacationManagerException {
        List<PublicHoliday> result = null;

        try {
            result = phApi.getPublicHolidaysByYear(year);
        } catch (final ApiException e) {
            throw ERRORS.getPublicHolidaysByYearException(year, e);
        }

        return result;
    }

    @DeleteMapping("/{publicHolidayId}")
    public PublicHoliday deletePublicHoliday(@PathVariable final Long publicHolidayId) throws VacationManagerException {
        PublicHoliday result = null;

        try {
            final PublicHoliday publicHoliday = phApi.getPublicHoliday(publicHolidayId);
            if (publicHoliday != null) {
                result = phApi.deletePublicHoliday(publicHoliday);
            } else {
                throw ERRORS.deleteMissingPublicHolidayException();
            }
        } catch (final ApiException e) {
            final String date = (result != null) ? result.getDay().format(DateUtils.LOCAL_DATE) : null;
            throw ERRORS.deletePublicHolidayException(date, e);
        }

        return result;
    }

}
