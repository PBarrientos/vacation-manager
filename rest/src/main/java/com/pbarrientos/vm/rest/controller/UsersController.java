
package com.pbarrientos.vm.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pbarrientos.vm.api.UsersApi;
import com.pbarrientos.vm.data.model.User;
import com.pbarrientos.vm.exception.ApiException;
import com.pbarrientos.vm.rest.error.VacationManagerException;
import com.pbarrientos.vm.rest.error.VacationManagerRestErrors;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@RestController
@RequestMapping("/users")
public class UsersController {

    private static final VacationManagerRestErrors ERRORS = VacationManagerRestErrors.INSTANCE;

    @Autowired
    private UsersApi usersApi;

    @GetMapping(value = "/")
    public List<User> getAllUsers() throws VacationManagerException {
        List<User> result = null;

        try {
            result = usersApi.getAllUsers();
        } catch (final ApiException e) {
            throw ERRORS.allUsersException(e);
        }

        return result;
    }

    @PostMapping(value = "/", consumes = "application/json")
    public ResponseEntity<String> addUser(@RequestBody final User user) throws VacationManagerException {

        //TODO: Validation of the team ?

        try {
            usersApi.saveUser(user);
        } catch (final ApiException e) {
            throw ERRORS.saveUserException(user.getIdentifier(), e);
        }

        return ResponseEntity.ok("The user has been saved without errors");
    }

    @PostMapping(value = "/changePassword/{userId}")
    public ResponseEntity<String> changeUserPassword(@PathVariable final Long userId, @RequestBody final String newPassword)
            throws VacationManagerException {

        User user = null;
        try {
            user = usersApi.getUser(userId);

            if (user == null) {
                return ResponseEntity.badRequest().body("The user doesn't exist");
            }

            usersApi.changePassword(user, newPassword);
        } catch (final ApiException e) {
            final String username = (user != null) ? user.getIdentifier() : "";
            throw ERRORS.changePasswordException(username, e);
        }

        return ResponseEntity.ok("The password have been changed");
    }

    @GetMapping(value = "/{userId}")
    public User getUser(@PathVariable final Long userId) throws VacationManagerException {
        User result = null;

        try {
            result = usersApi.getUser(userId);
        } catch (final ApiException e) {
            throw ERRORS.getUserByIdException(userId, e);
        }

        return result;
    }

    @GetMapping(value = "/team/{teamId}")
    public List<User> getUsersByTeam(@PathVariable final Long teamId) throws VacationManagerException {
        List<User> result = null;

        try {
            result = usersApi.getAllUsersByTeam(teamId);
        } catch (final ApiException e) {
            throw ERRORS.getUsersByTeamException(teamId, e);
        }

        return result;
    }

    @DeleteMapping(value = "/{userId}")
    public User deleteUser(@PathVariable final Long userId) throws VacationManagerException {
        User result = null;

        try {
            final User user = usersApi.getUser(userId);
            if (user != null) {
                result = usersApi.deleteUser(user);
            } else {
                throw ERRORS.deleteMissingUserException();
            }
        } catch (final ApiException e) {
            final String username = (result != null) ? result.getIdentifier() : "";
            throw ERRORS.deleteUserException(username, e);
        }

        return result;
    }

}
