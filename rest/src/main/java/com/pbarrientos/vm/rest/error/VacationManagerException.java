
package com.pbarrientos.vm.rest.error;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public class VacationManagerException extends Exception {

    private static final long serialVersionUID = 1L;

    public VacationManagerException() {
        super();
    }

    public VacationManagerException(final String message) {
        super(message);
    }

    public VacationManagerException(final Throwable cause) {
        super(cause);
    }

    public VacationManagerException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public VacationManagerException(final String message,
        final Throwable cause,
        final boolean enableSuppression,
        final boolean writableStackTrace) {

        super(message, cause, enableSuppression, writableStackTrace);
    }

}
