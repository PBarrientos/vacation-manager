
package com.pbarrientos.vm.rest.dto;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public class ErrorDto {

    private String code;
    private String message;

    public ErrorDto() {

    }

    /**
     * @param code the code of the error
     * @param message the description of the error
     */
    public ErrorDto(final String code, final String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

}
