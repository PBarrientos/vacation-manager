
package com.pbarrientos.vm.rest.error;

import org.jboss.logging.Messages;
import org.jboss.logging.annotations.Cause;
import org.jboss.logging.annotations.Message;
import org.jboss.logging.annotations.MessageBundle;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@MessageBundle(projectCode = "VM.")
public interface VacationManagerRestErrors {

    VacationManagerRestErrors INSTANCE = Messages.getBundle(VacationManagerRestErrors.class);

    // Public User Rest Errors: Code > 1100
    @Message(id = 1101, value = "There was an error retrieving the list of the users")
    VacationManagerException allUsersException(@Cause Exception e);

    @Message(id = 1102, value = "There was an error retrieving the user with ID %f")
    VacationManagerException getUserByIdException(Long userId, @Cause Exception e);

    @Message(id = 1103, value = "There was an error retrieving the users for the team with ID %f")
    VacationManagerException getUsersByTeamException(Long teamId, @Cause Exception e);

    @Message(id = 1104, value = "There was an error changing the password for the user %s")
    VacationManagerException changePasswordException(String username, @Cause Exception e);

    @Message(id = 1105, value = "There was an error saving the user %s")
    VacationManagerException saveUserException(String username, @Cause Exception e);

    @Message(id = 1106, value = "The user that you want to delete doesn't exist")
    VacationManagerException deleteMissingUserException();

    @Message(id = 1107, value = "There was an error deleting the user %s")
    VacationManagerException deleteUserException(String username, @Cause Exception e);

    // Public Vacation Rest Errors: Code > 1200
    @Message(id = 1201, value = "There was an error retrieving the list of the vacations")
    VacationManagerException allVacationsException(@Cause Exception e);

    @Message(id = 1202, value = "There was an error retrieving the vacation with ID %f")
    VacationManagerException getVacationByIdException(Long vacationId, @Cause Exception e);

    @Message(id = 1203, value = "There was an error retrieving the vacations for the user with ID %f")
    VacationManagerException getVacationsByUserException(Long userId, @Cause Exception e);

    @Message(id = 1204, value = "There was an error retrieving the vacations for the team with ID %f")
    VacationManagerException getVacationsByTeamException(Long teamId, @Cause Exception e);

    @Message(id = 1205, value = "There was an error saving the vacation for the user %s from %s until %s")
    VacationManagerException saveVacationException(String username, String from, String to, @Cause Exception e);

    @Message(id = 1206, value = "The vacation that you want to delete doesn't exist")
    VacationManagerException deleteMissingVacationException();

    @Message(id = 1207, value = "There was an error deleting the vacation for the user %s from %s to %s")
    VacationManagerException deleteVacationException(String username, String from, String to, @Cause Exception e);

    // Public Teams Rest Errors: Codes > 1300
    @Message(id = 1301, value = "There was an error retrieving the list of the teams")
    VacationManagerException allTeamsException(@Cause Exception e);

    @Message(id = 1302, value = "There was an error retrieving the team with ID: %f")
    VacationManagerException getTeamByIdException(Long team, @Cause Exception e);

    @Message(id = 1303, value = "There was an error saving the team %s")
    VacationManagerException saveTeamException(String team, @Cause Exception e);

    // Public Holidays Rest Errors: Code > 1400
    @Message(id = 1401, value = "There was an error retrieving the list of the public holidays")
    VacationManagerException allPublicHolidaysException(@Cause Exception e);

    @Message(id = 1402, value = "There was an error retrieving the public holiday with ID %f")
    VacationManagerException getPublicHolidayByIdException(Long publicHolidayId, @Cause Exception e);

    @Message(id = 1403, value = "There was an error retrieving the public holidays for the year %d")
    VacationManagerException getPublicHolidaysByYearException(Integer year, @Cause Exception e);

    @Message(id = 1404, value = "There was an error saving the public holiday for %s")
    VacationManagerException savePublicHolidayException(String date, @Cause Exception e);

    @Message(id = 1405, value = "The public holiday that you want to delete doesn't exist")
    VacationManagerException deleteMissingPublicHolidayException();

    @Message(id = 1406, value = "There was an error deleting the public holiday for %s")
    VacationManagerException deletePublicHolidayException(String date, @Cause Exception e);

}
