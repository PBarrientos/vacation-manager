
package com.pbarrientos.vm.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pbarrientos.vm.api.VacationApi;
import com.pbarrientos.vm.data.model.Vacation;
import com.pbarrientos.vm.exception.ApiException;
import com.pbarrientos.vm.rest.error.VacationManagerException;
import com.pbarrientos.vm.rest.error.VacationManagerRestErrors;
import com.pbarrientos.vm.rest.utils.DateUtils;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@RestController
@RequestMapping("/vacations")
public class VacationsController {

    private static final VacationManagerRestErrors ERRORS = VacationManagerRestErrors.INSTANCE;

    @Autowired
    private VacationApi vacationApi;

    @GetMapping(value = "/")
    public List<Vacation> getAllVacations() throws VacationManagerException {
        List<Vacation> result = null;

        try {
            result = vacationApi.getAllVacations();
        } catch (final ApiException e) {
            throw ERRORS.allVacationsException(e);
        }

        return result;
    }

    @PostMapping(value = "/", consumes = "application/json")
    public ResponseEntity<String> addVacation(@RequestBody final Vacation vacation) throws VacationManagerException {

        //TODO: Validation of the team ?

        try {
            vacationApi.saveVacation(vacation);
        } catch (final ApiException e) {
            final String username = (vacation != null && vacation.getUser() != null) ? vacation.getUser().getIdentifier() : "";
            final String from =
                    (vacation != null && vacation.getUser() != null) ? vacation.getStartDate().format(DateUtils.LOCAL_DATE) : null;
            final String to =
                    (vacation != null && vacation.getUser() != null) ? vacation.getEndDate().format(DateUtils.LOCAL_DATE) : null;
            throw ERRORS.saveVacationException(username, from, to, e);
        }

        return ResponseEntity.ok("The vacation has been saved without errors");
    }

    @GetMapping(value = "/{vacationId}")
    public Vacation getVacation(@PathVariable final Long vacationId) throws VacationManagerException {
        Vacation result = null;

        try {
            result = vacationApi.getVacation(vacationId);
        } catch (final ApiException e) {
            throw ERRORS.getVacationByIdException(vacationId, e);
        }

        return result;
    }

    @GetMapping(value = "/user/{userId}")
    public List<Vacation> getVacationsByUser(@PathVariable final Long userId) throws VacationManagerException {
        List<Vacation> result = null;

        try {
            result = vacationApi.getVacationsByUser(userId);
        } catch (final ApiException e) {
            throw ERRORS.getVacationsByUserException(userId, e);
        }

        return result;
    }

    @GetMapping(value = "/team/{teamId}")
    public List<Vacation> getVacationsByTeam(@PathVariable final Long teamId) throws VacationManagerException {
        List<Vacation> result = null;

        try {
            result = vacationApi.getVacationsByTeam(teamId);
        } catch (final ApiException e) {
            throw ERRORS.getVacationsByTeamException(teamId, e);
        }

        return result;
    }

    @DeleteMapping(value = "/{vacationId}")
    public Vacation deleteVacation(@PathVariable final Long vacationId) throws VacationManagerException {
        Vacation result = null;

        try {
            final Vacation vacation = vacationApi.getVacation(vacationId);
            if (vacation != null) {
                result = vacationApi.deleteVacation(vacation);
            } else {
                throw ERRORS.deleteMissingVacationException();
            }
        } catch (final ApiException e) {
            final String username = (result != null && result.getUser() != null) ? result.getUser().getIdentifier() : "";
            final String from =
                    (result != null && result.getUser() != null) ? result.getStartDate().format(DateUtils.LOCAL_DATE) : null;
            final String to =
                    (result != null && result.getUser() != null) ? result.getEndDate().format(DateUtils.LOCAL_DATE) : null;

            throw ERRORS.deleteVacationException(username, from, to, e);
        }

        return result;
    }

}
