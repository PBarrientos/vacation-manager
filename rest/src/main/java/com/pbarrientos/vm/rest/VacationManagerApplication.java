
package com.pbarrientos.vm.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

import com.pbarrientos.vm.VacationManagerApiApplication;

/**
 * Main class of the application.
 *
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@SpringBootApplication
@Import(VacationManagerApiApplication.class)
public class VacationManagerApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(final SpringApplicationBuilder application) {
        return application.sources(VacationManagerApplication.class);
    }

    public static void main(final String[] args) {
        SpringApplication.run(VacationManagerApplication.class, args);
    }
}
