
package com.pbarrientos.vm.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pbarrientos.vm.api.TeamsApi;
import com.pbarrientos.vm.data.model.Team;
import com.pbarrientos.vm.exception.ApiException;
import com.pbarrientos.vm.rest.error.VacationManagerException;
import com.pbarrientos.vm.rest.error.VacationManagerRestErrors;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
@RestController
@RequestMapping("/teams")
public class TeamsController {

    private static final VacationManagerRestErrors ERRORS = VacationManagerRestErrors.INSTANCE;

    @Autowired
    private TeamsApi teamsApi;

    @GetMapping(value = "/")
    public List<Team> getAllTeams() throws VacationManagerException {
        List<Team> result = null;

        try {
            result = teamsApi.getAllTeams();
        } catch (final ApiException e) {
            throw ERRORS.allTeamsException(e);
        }

        return result;
    }

    @PostMapping(value = "/", consumes = "application/json")
    public ResponseEntity<String> addTeam(@RequestBody final Team team) throws VacationManagerException {

        //TODO: Validation of the team ?

        try {
            teamsApi.saveTeam(team);
        } catch (final ApiException e) {
            throw ERRORS.saveTeamException(team.getName(), e);
        }

        return ResponseEntity.ok("The team has been saved without errors");
    }

    @GetMapping(value = "/{teamId}")
    public Team getTeam(@PathVariable final Long teamId) throws VacationManagerException {
        Team result = null;

        try {
            result = teamsApi.getTeam(teamId);
        } catch (final ApiException e) {
            throw ERRORS.getTeamByIdException(teamId, e);
        }

        return result;
    }

}
