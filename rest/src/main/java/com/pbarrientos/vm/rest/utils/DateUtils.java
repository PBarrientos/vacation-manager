
package com.pbarrientos.vm.rest.utils;

import java.time.format.DateTimeFormatter;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public final class DateUtils {

    public static final DateTimeFormatter LOCAL_DATE = InternalDateUtils.getLocalDate();

    private DateUtils() {
    }

    // Class that implement the logic
    static final class InternalDateUtils {

        private InternalDateUtils() {

        }

        public static DateTimeFormatter getLocalDate() {
            return DateTimeFormatter.ofPattern("dd/MM/yyyy");
        }

    }

}
