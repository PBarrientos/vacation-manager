
    create table T_VM_M_PUBLIC_HOLIDAYS (
       id bigint not null auto_increment,
        LAST_UPDATED datetime not null,
        version bigint,
        day date not null,
        description varchar(100),
        primary key (id)
    );

    create table T_VM_M_TEAMS (
       id bigint not null auto_increment,
        LAST_UPDATED datetime not null,
        version bigint,
        avatar varchar(2),
        name varchar(255) not null,
        PHOTO_URL varchar(255),
        primary key (id)
    );

    create table T_VM_M_USERS (
       id bigint not null auto_increment,
        LAST_UPDATED datetime not null,
        version bigint,
        attempts integer not null,
        birthdate date,
        city varchar(20),
        country varchar(20),
        email varchar(255),
        identifier varchar(20) not null,
        IS_ENABLED bit not null,
        LAST_LOGIN datetime,
        locked bit not null,
        name varchar(20) not null,
        password varchar(255) not null,
        phone varchar(255),
        photo varchar(255),
        surname varchar(50) not null,
        VACATION_DAYS integer,
        role_id bigint not null,
        TEAM_ID bigint,
        primary key (id)
    );

    create table T_VM_M_VACATIONS (
       id bigint not null auto_increment,
        LAST_UPDATED datetime not null,
        version bigint,
        approved bit not null,
        END_DATE date not null,
        managed bit not null,
        START_DATE date not null,
        year integer not null,
        USER_ID bigint,
        primary key (id)
    );

    create table T_VM_P_ROLES (
       id bigint not null,
        code varchar(50),
        description longtext,
        primary key (id)
    );

    alter table T_VM_M_PUBLIC_HOLIDAYS 
       add constraint UK_PUBLIC_HOLIDAYS_DAYS unique (day);

    alter table T_VM_M_TEAMS 
       add constraint UK_TEAMS_NAME unique (name);

    alter table T_VM_M_USERS 
       add constraint UK_USERS_EMAIL unique (email);

    alter table T_VM_M_USERS 
       add constraint UK_USERS_IDENTIFIER unique (identifier);

    alter table T_VM_P_ROLES 
       add constraint UK_ROLES_CODE unique (code);

    alter table T_VM_M_USERS 
       add constraint FK_USERS_ROLES 
       foreign key (role_id) 
       references T_VM_P_ROLES (id);

    alter table T_VM_M_USERS 
       add constraint FK_USERS_TEAMS 
       foreign key (TEAM_ID) 
       references T_VM_M_TEAMS (id);

    alter table T_VM_M_VACATIONS 
       add constraint FK_VACATIONS_USERS 
       foreign key (USER_ID) 
       references T_VM_M_USERS (id);
