
package com.pbarrientos.vm.rest.test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

/**
 * General tests for VacationManagerApplication. Used for test configuration
 * 
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@SpringBootTest
public class VacationManagerRestTests extends AbstractTestNGSpringContextTests {

    /**
     * Test that checks that the application context is loaded
     */
    @Test
    public void contextLoads() {

    }
}
