
package com.pbarrientos.vm.rest.test.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.pbarrientos.vm.data.enums.EnumRoles;
import com.pbarrientos.vm.data.model.Role;
import com.pbarrientos.vm.data.model.User;
import com.pbarrientos.vm.rest.test.base.BaseIntegrationTest;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public class UsersRestTests extends BaseIntegrationTest {

    @Test
    public void testAllUsers() throws Exception {
        final ResultActions request = mvc.perform(get("/users/")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();
        final List<User> result = getMapper().readValue(jsonResult, getCollectionFor(User.class));

        Assert.assertNotNull(result);
        Assert.assertTrue(result.size() == 2);
    }

    @Test
    public void testGetUser() throws Exception {
        final ResultActions request = mvc.perform(get("/users/2")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();
        final User result = getMapper().readValue(jsonResult, User.class);

        Assert.assertNotNull(result);
        Assert.assertEquals(result.getIdentifier(), "pb");
    }

    @Test
    public void testGetUsersByTeam() throws Exception {
        final ResultActions request = mvc.perform(get("/users/team/1")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();
        final List<User> result = getMapper().readValue(jsonResult, getCollectionFor(User.class));

        Assert.assertNotNull(result);
        Assert.assertTrue(result.size() == 2);
    }

    @Test
    public void testGetUsersByInexistentTeam() throws Exception {
        final ResultActions request = mvc.perform(get("/users/team/10")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();

        Assert.assertEquals(jsonResult, "[]");
    }

    @Test
    public void testChangePassword() throws Exception {
        mvc.perform(post("/users/changePassword/2").content("newPassword"))
            .andExpect(status().isOk())
            .andExpect(content().string("The password have been changed"));
    }

    @Test
    public void testChangePasswordForInexistentUser() throws Exception {
        mvc.perform(post("/users/changePassword/6").content("newPassword"))
            .andExpect(status().isBadRequest())
            .andExpect(content().string("The user doesn't exist"));
    }

    @Test
    public void testNewUser() throws Exception {
        final Role role = new Role(EnumRoles.USER);
        final User user = new User();
        user.setIdentifier("Test1");
        user.setPassword("password");
        user.setName("Test");
        user.setSurname("Test");
        user.setBirthdate(LocalDate.now());
        user.setIsEnabled(true);
        user.setLocked(false);
        user.setAttempts(0);
        user.setRole(role);
        user.setLastUpdated(LocalDateTime.now());

        final String userJson = getMapper().writeValueAsString(user);

        mvc.perform(post("/users/").contentType(MediaType.APPLICATION_JSON).content(userJson))
            .andExpect(status().isOk())
            .andExpect(content().string("The user has been saved without errors"));
    }

    @Test
    public void testAddExistingUser() throws Exception {
        final Role role = new Role(EnumRoles.USER);
        final User user = new User();
        user.setIdentifier("pb");
        user.setPassword("password");
        user.setName("Test");
        user.setSurname("Test");
        user.setBirthdate(LocalDate.now());
        user.setIsEnabled(true);
        user.setLocked(false);
        user.setAttempts(0);
        user.setRole(role);
        user.setLastUpdated(LocalDateTime.now());

        final String userJson = getMapper().writeValueAsString(user);

        mvc.perform(post("/users/").contentType(MediaType.APPLICATION_JSON).content(userJson))
            .andExpect(status().is4xxClientError());
    }

    @Test
    public void testDeleteUser() throws Exception {
        final ResultActions request = mvc.perform(delete("/users/2")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();

        final User user = getMapper().readValue(jsonResult, User.class);

        Assert.assertNotNull(user);
        Assert.assertEquals(user.getIdentifier(), "pb");
    }

    @Test
    public void testDeleteExistingUser() throws Exception {
        mvc.perform(delete("/users/20")).andExpect(status().is4xxClientError());
    }
}
