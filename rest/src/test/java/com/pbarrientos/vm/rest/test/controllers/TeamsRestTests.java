
package com.pbarrientos.vm.rest.test.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.pbarrientos.vm.data.model.Team;
import com.pbarrientos.vm.rest.test.base.BaseIntegrationTest;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public class TeamsRestTests extends BaseIntegrationTest {

    @Test
    public void testAllTeams() throws Exception {
        final ResultActions request = mvc.perform(get("/teams/")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();
        final List<Team> result = getMapper().readValue(jsonResult, getCollectionFor(Team.class));

        Assert.assertNotNull(result);
        Assert.assertTrue(result.size() == 1);
    }

    @Test
    public void testGetTeam() throws Exception {
        final ResultActions request = mvc.perform(get("/teams/1")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();
        final Team result = getMapper().readValue(jsonResult, Team.class);

        Assert.assertNotNull(result);
    }

    @Test
    public void testAddTeam() throws Exception {
        final Team team = new Team();
        team.setName("Test Team");

        final String jsonString = getMapper().writeValueAsString(team);

        mvc.perform(post("/teams/").contentType(MediaType.APPLICATION_JSON).content(jsonString))
            .andExpect(status().isOk())
            .andExpect(content().string("The team has been saved without errors"));
    }

    @Test
    public void testAddExistingTeam() throws Exception {
        final Team team = new Team();
        team.setName("Crossfield");

        final String jsonString = getMapper().writeValueAsString(team);

        mvc.perform(post("/teams/").contentType(MediaType.APPLICATION_JSON).content(jsonString))
            .andExpect(status().is4xxClientError());
    }

}
