
package com.pbarrientos.vm.rest.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.StringUtils;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version
 * @since
 */
public class HashPassword {

    /**
     * Method that initializes a Java process that will ask you for a password
     * and returns it encrypted
     * 
     * @param args arguments passed
     * @throws IOException if any error occur
     */
    public static void main(final String[] args) throws IOException {
        BCryptPasswordEncoder encoder;
        InputStreamReader stream;
        BufferedReader reader;
        String line;
        String hash;

        encoder = new BCryptPasswordEncoder(5);
        stream = new InputStreamReader(System.in);
        reader = new BufferedReader(stream);

        System.out.println("Enter passwords to be hashed or <ENTER> to quit");

        line = reader.readLine();
        while (StringUtils.hasText(line)) {
            hash = encoder.encode(line);
            System.out.println(hash);
            line = reader.readLine();
        }

    }

}
