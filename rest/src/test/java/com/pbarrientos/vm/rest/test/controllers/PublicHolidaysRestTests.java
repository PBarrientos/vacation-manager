
package com.pbarrientos.vm.rest.test.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.pbarrientos.vm.data.model.PublicHoliday;
import com.pbarrientos.vm.rest.test.base.BaseIntegrationTest;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public class PublicHolidaysRestTests extends BaseIntegrationTest {

    @Test
    public void testAllPublicHolidays() throws Exception {
        final ResultActions request = mvc.perform(get("/public-holidays/")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();
        final List<PublicHoliday> result = getMapper().readValue(jsonResult, getCollectionFor(PublicHoliday.class));

        Assert.assertNotNull(result);
        Assert.assertEquals(result.size(), 1);
    }

    @Test
    public void testGetPublicHolidays() throws Exception {
        final ResultActions request = mvc.perform(get("/public-holidays/1")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();

        Assert.assertNotEquals(jsonResult, "");
    }

    @Test
    public void testGetPublicHolidaysByYear() throws Exception {
        final ResultActions request = mvc.perform(get("/public-holidays/year/2017")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();
        final List<PublicHoliday> result = getMapper().readValue(jsonResult, getCollectionFor(PublicHoliday.class));

        Assert.assertNotNull(result);
        Assert.assertEquals(result.size(), 1);
    }

    @Test
    public void testGetPublicHolidaysByInexistentYear() throws Exception {
        final ResultActions request = mvc.perform(get("/public-holidays/year/10")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();

        Assert.assertEquals(jsonResult, "[]");
    }

    @Test
    public void testNewPublicHoliday() throws Exception {
        final PublicHoliday publicHoliday = new PublicHoliday();
        publicHoliday.setDay(LocalDate.of(2018, 2, 28));
        publicHoliday.setDescription("Andalucia's day");

        final String phJson = getMapper().writeValueAsString(publicHoliday);

        mvc.perform(post("/public-holidays/").contentType(MediaType.APPLICATION_JSON).content(phJson))
            .andExpect(status().isOk())
            .andExpect(content().string("The public holiday has been saved without errors"));
    }

    @Test
    public void testAddExistingPublicHoliday() throws Exception {
        final PublicHoliday publicHoliday = new PublicHoliday();
        publicHoliday.setDay(LocalDate.of(2017, 1, 1));
        publicHoliday.setDescription("New Year");

        final String phJson = getMapper().writeValueAsString(publicHoliday);

        mvc.perform(post("/public-holidays/").contentType(MediaType.APPLICATION_JSON).content(phJson))
            .andExpect(status().is4xxClientError());
    }

    @Test
    public void testDeletePublicHoliday() throws Exception {
        final ResultActions request = mvc.perform(delete("/public-holidays/1")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();

        final PublicHoliday user = getMapper().readValue(jsonResult, PublicHoliday.class);

        Assert.assertNotNull(user);
    }

    @Test
    public void testDeleteMissingPublicHoliday() throws Exception {
        mvc.perform(delete("/public-holidays/20")).andExpect(status().is4xxClientError());
    }

}
