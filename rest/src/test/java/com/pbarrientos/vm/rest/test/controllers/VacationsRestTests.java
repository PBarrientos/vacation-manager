
package com.pbarrientos.vm.rest.test.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.pbarrientos.vm.data.model.User;
import com.pbarrientos.vm.data.model.Vacation;
import com.pbarrientos.vm.rest.test.base.BaseIntegrationTest;

/**
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.1.0
 * @since 0.1.0
 */
public class VacationsRestTests extends BaseIntegrationTest {

    @Test
    public void testAllVacations() throws Exception {
        final ResultActions request = mvc.perform(get("/vacations/")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();
        final List<Vacation> result = getMapper().readValue(jsonResult, getCollectionFor(Vacation.class));

        Assert.assertNotNull(result);
        Assert.assertTrue(result.size() == 1);
    }

    @Test
    public void testGetVacation() throws Exception {
        final ResultActions request = mvc.perform(get("/vacations/1")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();
        final Vacation result = getMapper().readValue(jsonResult, Vacation.class);

        Assert.assertNotNull(result);
    }

    @Test
    public void testGetMissingVacation() throws Exception {
        final ResultActions request = mvc.perform(get("/vacations/10")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();

        Assert.assertEquals(jsonResult, "");
    }

    @Test
    public void testGetVacationsByTeam() throws Exception {
        final ResultActions request = mvc.perform(get("/vacations/team/1")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();
        final List<Vacation> result = getMapper().readValue(jsonResult, getCollectionFor(Vacation.class));

        Assert.assertNotNull(result);
        Assert.assertTrue(result.size() == 1);
    }

    @Test
    public void testGetVacationsByInexistentTeam() throws Exception {
        final ResultActions request = mvc.perform(get("/vacations/team/10")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();

        Assert.assertEquals(jsonResult, "[]");
    }

    @Test
    public void testGetVacationsByUser() throws Exception {
        final ResultActions request = mvc.perform(get("/vacations/user/1")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();
        final List<Vacation> result = getMapper().readValue(jsonResult, getCollectionFor(Vacation.class));

        Assert.assertNotNull(result);
        Assert.assertEquals(result.size(), 1);
    }

    @Test
    public void testGetVacationsByInexistentUser() throws Exception {
        final ResultActions request = mvc.perform(get("/vacations/user/10")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();

        Assert.assertEquals(jsonResult, "[]");
    }

    @Test
    public void testNewVacation() throws Exception {

        final String userJson = mvc.perform(get("/users/2")).andExpect(status().isOk())
            .andReturn().getResponse().getContentAsString();

        final User user = getMapper().readValue(userJson, User.class);

        final Vacation vacation = new Vacation();
        vacation.setUser(user);
        vacation.setStartDate(LocalDate.of(2017, 2, 1));
        vacation.setEndDate(LocalDate.of(2017, 2, 5));
        vacation.setYear(2017);

        final String vacationJson = getMapper().writeValueAsString(vacation);

        mvc.perform(post("/vacations/").contentType(MediaType.APPLICATION_JSON).content(vacationJson))
            .andExpect(status().isOk())
            .andExpect(content().string("The vacation has been saved without errors"));
    }

    // Ignored until validation and vacations creation are completely well defined
    @Test(enabled = false)
    public void testIncorrectNewVacation() throws Exception {

        final String userJson = mvc.perform(get("/users/2")).andExpect(status().isOk())
            .andReturn().getResponse().getContentAsString();

        final User user = getMapper().readValue(userJson, User.class);

        final Vacation vacation = new Vacation();
        vacation.setStartDate(LocalDate.of(2017, 2, 1));
        vacation.setEndDate(LocalDate.of(2017, 2, 5));

        final String vacationJson = getMapper().writeValueAsString(vacation);

        mvc.perform(post("/vacations/").contentType(MediaType.APPLICATION_JSON).content(vacationJson))
            .andExpect(status().is4xxClientError());
    }

    @Test
    public void testDeleteVacation() throws Exception {
        final ResultActions request = mvc.perform(delete("/vacations/1")).andExpect(status().isOk());

        final String jsonResult = request.andReturn().getResponse().getContentAsString();

        final Vacation vacation = getMapper().readValue(jsonResult, Vacation.class);

        Assert.assertNotNull(vacation);
    }

    @Test
    public void testDeleteNotExistingVacation() throws Exception {
        mvc.perform(delete("/vacations/20")).andExpect(status().is4xxClientError());
    }

}
