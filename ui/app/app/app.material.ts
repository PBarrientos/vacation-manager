import { NgModule } from '@angular/core';
import { MdCheckboxModule, MdInputModule, MdRadioModule, MdSelectModule, MdSliderModule, MdMenuModule, MdSidenavModule, MdToolbarModule, MdListModule, MdCardModule, MdButtonModule, MdChipsModule, MdIconModule, MdProgressSpinnerModule, MdDialogModule, MdTooltipModule } from '@angular/material';

@NgModule({
  imports: [MdCheckboxModule, MdInputModule, MdRadioModule, MdSelectModule, MdSliderModule, MdMenuModule, MdSidenavModule, MdToolbarModule, MdListModule, MdCardModule, MdButtonModule, MdChipsModule, MdIconModule, MdProgressSpinnerModule, MdDialogModule, MdTooltipModule],
  exports: [MdCheckboxModule, MdInputModule, MdRadioModule, MdSelectModule, MdSliderModule, MdMenuModule, MdSidenavModule, MdToolbarModule, MdListModule, MdCardModule, MdButtonModule, MdChipsModule, MdIconModule, MdProgressSpinnerModule, MdDialogModule, MdTooltipModule]
})
export class AppMaterialModule {  }
