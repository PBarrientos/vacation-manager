
package com.pbarrientos.vm.ui;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * Web configuration for UI module
 * 
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@Configuration
@EnableWebMvc
public class VacationManagerUIConfiguration extends WebMvcConfigurerAdapter {

    @Override
    public void configureDefaultServletHandling(final DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        registry.addViewController("/index.html").setViewName("index");
    }

    @Bean
    public InternalResourceViewResolver viewResolver() {
        final InternalResourceViewResolver vr = new InternalResourceViewResolver("/WEB-INF/", ".html");
        return vr;
    }

}
