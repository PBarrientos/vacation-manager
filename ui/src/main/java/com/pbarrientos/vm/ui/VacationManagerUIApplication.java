
package com.pbarrientos.vm.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Main class of the UI application.
 *
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version 0.0.1
 * @since 0.0.1
 */
@SpringBootApplication
public class VacationManagerUIApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(final SpringApplicationBuilder application) {
        return application.sources(VacationManagerUIApplication.class);
    }

    public static void main(final String[] args) {
        SpringApplication.run(VacationManagerUIApplication.class, args);
    }
}
