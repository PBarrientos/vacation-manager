
package com.pbarrientos.vm.ui.test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

/**
 * General tests for VacationManagerApplication. Used for test configuration
 * 
 * @author <a href="mailto:pablo.barrientos.13@gmail.com">Pablo Barrientos</a>
 * @version
 * @since
 */
@SpringBootTest
public class VacationManagerApplicationTests extends AbstractTestNGSpringContextTests {

    /**
     * Test that checks that the application context is loaded
     */
    @Test
    public void contextLoads() {

    }

}
